"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Room {
    constructor(roomNo) {
        this.no = roomNo;
        this.floor = roomNo.substr(0, 1);
        this.isBooked = false;
    }
    book() {
        this.isBooked = true;
    }
}
exports.Room = Room;
