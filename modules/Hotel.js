"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Hotel {
    constructor(floorCount, roomPerFloorCount) {
        this.floorCount = floorCount;
        this.roomPerFloorCount = roomPerFloorCount;
        this.roomCount = floorCount * roomPerFloorCount;
    }
}
exports.Hotel = Hotel;
